FROM ubuntu:16.04
RUN apt-get -qq update && apt-get -qq -y install curl && apt-get install bzip2
RUN curl -LO http://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh
RUN bash Miniconda-latest-Linux-x86_64.sh -p /miniconda -b
RUN rm Miniconda-latest-Linux-x86_64.sh
ENV PATH=/miniconda/bin:${PATH}
RUN conda update -y conda --yes
RUN conda install numpy --yes  
RUN conda install scipy --yes  
RUN conda install pandas --yes  
RUN conda install qtpy --yes  
RUN conda install seaborn --yes  
RUN conda install scikit-learn --yes  
RUN conda install spyder --yes  
RUN conda install sqlite --yes  
RUN conda install theano --yes  
RUN conda install tensorflow --yes  
RUN conda install h5py --yes  
RUN conda install keras --yes  
RUN conda install zlib --yes  
RUN conda install matplotlib --yes  
RUN conda install jupyter --yes  
 
